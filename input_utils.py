'''
File containing methods for loading inputs based on the models used
'''
from __future__ import print_function

import sys
import numpy as np

def get_input(args, word_emb_model, input_file):
    '''loads input dataset based on model selected'''
    # Encode inputs based on annotations in the file
    tokens, instances, labels, max_len = get_sent_input(word_emb_model,
                                                        input_file)

    # Determine type of max length to use i.e. specified or auto
    max_len = args.max_len if args.max_len else max_len
    print("Using max sentence length:", max_len)
    # Based on max length pad sentences to max length for efficiency
    return get_rnn_input(max_len, tokens, instances, labels)

def get_sent_input(word_emb_model, input_file):
    '''loads input dataset for rnn models'''
    print("processing file: {}".format(input_file))
    tokens, instances, labels = [], [], []
    doc_tokens, doc_instances, doc_labels = [], [], []
    for line in open(input_file):
        if len(line.split()) == 2:
            token = line.split()[0]
            label = line.split()[1]
            doc_tokens.append(token)
            doc_instances.append(word_emb_model[token])
            if label.startswith('O'):
                doc_labels.append(np.array([1, 0, 0]))
            elif label.startswith('B'):
                doc_labels.append(np.array([0, 1, 0]))
            elif label.startswith('I'):
                doc_labels.append(np.array([0, 0, 1]))
            else:
                print("Invalid tag {} found for word {}".format(label, token))
        else:
            assert len(doc_tokens) == len(doc_instances) == len(doc_labels)
            tokens.append(doc_tokens)
            instances.append(np.array(doc_instances))
            labels.append(np.array(doc_labels))
            doc_tokens, doc_instances, doc_labels = [], [], []
    assert len(tokens) == len(instances) == len(labels)
    max_len = max([len(x) for x in tokens])
    print("Max length", max_len)
    return tokens, instances, labels, max_len

def get_rnn_input(max_len, sentences, instances, labels):
    '''Pads to max length for RNN models for operational efficiency'''
    input_len = len(instances[0][0])
    num_classes = 3
    token_sets = []
    instns_sets = []
    label_sets = []
    seqlen_sets = []
    s_index = 0
    while s_index < len(sentences):
        s_tokens = sentences[s_index]
        s_instns = instances[s_index]
        s_labels = labels[s_index]
        if len(s_tokens) <= max_len:
            # Append till close to max length for efficiency
            while (s_index + 1 < len(sentences) and
                   len(s_tokens) + len(sentences[s_index+1]) <= max_len):
                s_index += 1
                s_tokens += sentences[s_index]
                s_instns = np.append(s_instns, instances[s_index], axis=0)
                s_labels = np.append(s_labels, labels[s_index], axis=0)
            s_index += 1
        elif len(s_tokens) > max_len:
            # If greater than max length, just break it at max length
            sentences[s_index] = s_tokens[max_len:]
            instances[s_index] = s_instns[max_len:]
            labels[s_index] = s_labels[max_len:]
            s_tokens = s_tokens[:max_len]
            s_instns = s_instns[:max_len]
            s_labels = s_labels[:max_len]
        assert len(s_tokens) == len(s_instns) == len(s_labels)
        # Add padding when short of length
        for _ in range(max_len - len(s_tokens)):
            padding = np.expand_dims(np.zeros(num_classes), axis=0)
            s_labels = np.append(s_labels, padding, axis=0)
            padding = np.expand_dims(np.zeros(input_len), axis=0)
            s_instns = np.append(s_instns, padding, axis=0)
        assert len(s_instns) == len(s_labels)
        token_sets.append(s_tokens)
        instns_sets.append(s_instns)
        label_sets.append(s_labels)
        seqlen_sets.append(len(s_tokens))
    sumtokens = sum([len(x) for x in token_sets])
    print("Total_tokens:{} Total_sents:{}".format(sumtokens, len(sentences)),
          "Token_T:[{}][?]".format(len(token_sets)),
          "Vector_T:[{}][{}][{}]".format(len(instns_sets), len(instns_sets[0]),
                                         len(instns_sets[0][0])),
          "Label_T:[{}][{}][{}]".format(len(label_sets), len(label_sets[0]),
                                        len(label_sets[0][0])))
    assert len(token_sets) == len(instns_sets) == len(label_sets) == len(seqlen_sets)
    return token_sets, np.asarray(instns_sets), np.asarray(label_sets), seqlen_sets
