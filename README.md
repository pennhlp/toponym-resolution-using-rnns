# Toponym resolution in scientific articles using RNNs
This project presents a system for Named Entity Recognition (NER) and Concept Resolution for extracting geographic location from scientific articles. The proposed system uses Recurrent Neural Network (RNN) models for performing the task of NER. The system allows the user to train and use five variants of RNN cells:
1) RNN 
2) Update Gate RNN (UG-RNN)
3) Gated Recurrent Units (GRU)
4) Long Short Term Memory units (LSTM)
5) LSTM with peepholes

# Installation
1) ```python``` tested with version 2.7
2) [geonames-services](https://github.com/amagge/geonames-service) for disabmiguation and normalization. Alternatively, you can use GeoNames's search API (with a few modifications in this system).

The system depends on a few python packages that can be installed using the command:
```
pip install --upgrade -r requirements.txt
```

# Training for Detection and Resolution
Both Detection and Resolution steps require that a trained model for detection is available.

Requirements:
- Directory containing BRAT annotated files i.e. corpus files containing article texts (.txt) and respective annotation files (.ann). You can extract the training files from the provided dataset and place the .ann and .txt files in the ```data/train``` directory.
- A file containing word embeddings i.e word vectors that can be loaded using the gensim model. You can download word embeddings trained on PubMed and Wikipedia articles(wikipedia-pubmed-and-PMC-w2v.bin) from http://bio.nlplab.org/ and place the bin file in the ```resources``` directory.

To train the model:
1) Generate the files required for training by running the following command. 
```
python gen_training_files.py
```
This will generate some files in the ```resources``` directory that are required for training and running the models. To look at more options for input and output paths, run the command with the ```-h``` or ```--help``` flag.

2) Train all the models ```BIRNN```, ```BIUGRNN```, ```BIGRU```, ```BILSTM```, ```BILSTMP``` by running the following command:
```
python train.py ALL
```
To train a specific model e.g. LSTM replace ALL with the type of model.
```
python train.py BILSTM
```
This will create model files in the model directory. To look at more options for inputs paths and hyperparameter configurations run the command with the ```-h```  or ```--help``` flag.

# Detection
If you wish to perform detection alone using the trained model, complete the steps 1 and 2 above and run the following command:
```
python run.py BILSTM det data/test/
```
You will find the outputs in the ```out``` directory. You can replace BILSTM with a trained model of your choice. To look at more options for input and output paths, run the command with the ```-h``` or ```--help``` flag.

# Resolution
Since resolution performs detection and disambiguation, complete the steps 1 and 2 above and then run the following command:
```
python run.py [BIRNN|BIUGRNN|BIGRU|BILSTM|BILSTMP] res data/test/
```
You will find the outputs in the ```out``` directory. To look at more options for input and output paths, run the command with the ```-h``` or ```--help``` flag.

# Citation
If you find this work useful you can cite it using:
```
@article{maggeISMB2018bidirectional,
  title={Bi-directional Recurrent Neural Network Models for Geographic Location Extraction in Biomedical Literature},
  author={Magge, Arjun and Weissenbacher, Davy and Sarker, Abeed and Scotch, Matthew and Gonzalez-Hernandez, Graciela},
  journal={Pacific Symposium on Biocomputing},
  publisher={World Scientific Publishing Company},
  year={2018}
}
```
