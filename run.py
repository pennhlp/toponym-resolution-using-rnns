'''
A Deep Neural network with two layers for independent classification
'''

from __future__ import print_function
import argparse
import pickle
import os
from os import listdir
from os.path import isfile, join
import tensorflow as tf
import numpy as np

from models import BiRNNModel
from train import HYPRM_FILE_NAME, MODEL_NAMES
from utils import (WordEmb, tokenize_document, get_entity_annotations,
                   write_annotations, read_annotations, get_mask)

def get_input_pmc(max_len, word_emb_model, input_file):
    '''loads input dataset based on model selected'''
    doc_tokens, _ = tokenize_document(input_file)
    return get_rnn_input(max_len, doc_tokens, word_emb_model)

def get_rnn_input(max_len, sentences, word_emb_model):
    '''Pads to max length for RNN models for operational efficiency'''
    instances = []
    for sents in sentences:
        sent_inst = []
        for token in sents:
            sent_inst.append(word_emb_model[token.text])
        instances.append(sent_inst)
    input_len = len(instances[0][0])
    token_sets = []
    instns_sets = []
    seqlen_sets = []
    s_index = 0
    while s_index < len(sentences):
        s_tokens = sentences[s_index]
        s_instns = instances[s_index]
        if len(s_tokens) <= max_len:
            # Append till close to max length for efficiency
            while (s_index + 1 < len(sentences) and
                   len(s_tokens) + len(sentences[s_index+1]) <= max_len):
                s_index += 1
                s_tokens += sentences[s_index]
                s_instns = np.append(s_instns, instances[s_index], axis=0)
            s_index += 1
        elif len(s_tokens) > max_len:
            # If greater than max length, just break it
            sentences[s_index] = s_tokens[max_len:]
            instances[s_index] = s_instns[max_len:]
            s_tokens = s_tokens[:max_len]
            s_instns = s_instns[:max_len]
        assert len(s_tokens) == len(s_instns)
        # Add padding
        for _ in range(max_len - len(s_tokens)):
            padding = np.expand_dims(np.zeros(input_len), axis=0)
            s_instns = np.append(s_instns, padding, axis=0)
        token_sets.append(s_tokens)
        instns_sets.append(s_instns)
        seqlen_sets.append(len(s_tokens))
    assert len(token_sets) == len(instns_sets) == len(seqlen_sets)
    return token_sets, np.asarray(instns_sets), seqlen_sets


def detect(args):
    '''Method for detection of entities and normalization'''
    word_emb = WordEmb(args)
    print("Loading model")
    # Model checkpoint path
    save_loc = join(args.save, join(args.model, args.model))
    # Load Hyperparams from disk
    hypr_loc = save_loc + "_" + HYPRM_FILE_NAME
    hyperparams = pickle.load(open(hypr_loc, "rb"))
    # Create model
    model = BiRNNModel(hyperparams)
    print("Starting session")
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        saver = tf.train.Saver()
        saver = tf.train.import_meta_graph(save_loc + '.meta')
        saver.restore(sess, save_loc)
        # load pubmed files
        pub_files = [f for f in listdir(args.dir) if isfile(join(args.dir, f)) and f.endswith(".txt")]
        for _, pubfile in enumerate(pub_files):
            pub_t, pub_v, pub_sl = get_input_pmc(hyperparams.max_len, word_emb,
                                                 join(args.dir, pubfile))
            pub_m = get_mask(pub_v.shape[0], pub_v.shape[1], pub_sl)
            pred = sess.run(model.prediction, feed_dict={model.input_x: np.asarray(pub_v),
                                                         model.length: np.asarray(pub_sl),
                                                         model.dropout: 1.0})
            pmid = pubfile.replace(".txt", "")
            # Pass last parameter as True if you want to write token predictions to file
            # You might want to do it for debugging purposes
            entities = get_entity_annotations(args.outdir, pub_t, pred, pub_m, pmid)
            write_annotations(args.outdir, entities, pmid, args.op == "res")

def disambiguate(args):
    '''Method for disambiguation'''
    # load pubmed files
    pub_files = [f for f in listdir(args.dir) if isfile(join(args.dir, f)) and f.endswith(".ann")]
    for _, pubfile in enumerate(pub_files):
        entities = read_annotations(join(args.dir, pubfile))
        pmid = pubfile.replace(".ann", "")
        write_annotations(args.outdir, entities, pmid, True)

def main():
    '''Main method : parse input arguments and run appropriate operation'''
    parser = argparse.ArgumentParser()
    # Input files
    parser.add_argument('model', type=str,
                        choices=MODEL_NAMES,
                        help="Model to be used")
    parser.add_argument('op', type=str, choices=["det", "dis", "res"],
                        help="Operation to be performed")
    parser.add_argument('dir', type=str,
                        help='Location to dir containing files to be annotated')
    parser.add_argument('--work_dir', type=str, default="resources/",
                        help="working directory containing resource files")
    parser.add_argument('--save', type=str, default="model/",
                        help="path to saved model to be loaded")
    parser.add_argument('--outdir', type=str, default="out/",
                        help='Output dir for annotated pubmed files.')
    parser.add_argument('--gpu', type=int, default=None,
                        help='GPU number')
    # Word Embeddings
    parser.add_argument('--emb_loc', type=str,
                        default='resources/wikipedia-pubmed-and-PMC-w2v.bin',
                        help='word2vec embedding location')
    parser.add_argument('--embvocab', type=int, default=-1,
                        help='load top n words in word emb. -1 for all.')
    args = parser.parse_args()

    # Choose GPU to run
    if args.gpu is not None:
        print("Using GPU resource", args.gpu)
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

    # Run
    if args.op in ["det", "res"]:
        detect(args)
    elif args.op == "dis":
        disambiguate(args)


if __name__ == '__main__':
    main()
