'''File for calculating performance scores for the resolution task'''
from __future__ import print_function

import sys
import argparse
import pickle
import random
import os
from os import listdir, makedirs
from os.path import join, exists
import codecs
import unidecode
import numpy as np
import requests
from requests.utils import quote

from utils import read_annotations

from utils import LOC_ANN_TAG, PRO_ANN_TAG

log_set = set()

def filter_annotations(pred_anns, gold_anns):
    '''Remove protein annotations'''
    invalid_pred = []
    for gold in gold_anns:
        # For every protein annotation
        if gold.atype == PRO_ANN_TAG:
            for pindex, pred in enumerate(pred_anns):
                if (gold.start <= pred.start <= gold.end or 
                    gold.start <= pred.end <= gold.end):
                    pred_anns[pindex] = False
                    invalid_pred.append(pred)
            pred_anns = [x for x in pred_anns if x]
    # print("Extraneous ranges:", [str(x.start)+":"+str(x.end) for x in gold_anns if x.atype == PRO_ANN_TAG])
    # print("Removed:", len(invalid_pred), "entities:", [x.text+":"+str(x.start) for x in invalid_pred])
    gold_anns = [x for x in gold_anns if x.atype == LOC_ANN_TAG]
    return pred_anns, gold_anns

def test_res(pred_entities, gold_entities, rfile, ann_file):
    '''Compared Geoname IDs from pred and gold entities'''
    correct, incorrect = 0, 0
    fp_en, fn_en, corr_en, incorr_en = [], [], [], []
    tpred_entities = pred_entities[:]
    for _, gold in enumerate(gold_entities):
        g_found = False
        for pindex, pred in enumerate(pred_entities):
            # Overlapping Named Entity
            if (gold.start <= pred.start <= gold.end or 
                    gold.start <= pred.end <= gold.end):
                g_found = True
                del pred_entities[pindex]
                if gold.geonameid != pred.geonameid:
                    # If no label in gold annotations, i.e. -1 then skip
                    if gold.geonameid != -1:
                        incorrect += 1
                        incorr_en.append(pred)
                else:
                    correct += 1
                    corr_en.append(pred)
        if not g_found:
            fn_en.append(gold)
    fp_en = pred_entities
    pred_entities = tpred_entities
    # print("False Positives", [x.text+":"+str(x.start) for x in fp_en])
    precision = 1.0 * correct / (len(pred_entities) + 0.000001)
    recall = 1.0 * correct / (len(gold_entities) + 0.000001)
    f1sc = 2.0 * precision * recall / (precision + recall + 0.000001)
    print("correct:{}, incorrect:{}".format(correct, incorrect),
          "total_gold:{}, total_pred:{}".format(len(gold_entities), len(pred_entities)))
    print("Precision:{}, Recall:{}, F1:{}\n".format(precision, recall, f1sc))
    return correct, incorrect

def test(args):
    '''Test method'''
    correct, incorrect = 0, 0
    ann_files = [f for f in listdir(args.gold) if f.endswith(".ann")]#[:1]
    rfile = open(join(args.log, args.pred.replace("/", "_") + ".log"), 'w')
    total_gold, total_pred = 0, 0
    for index, ann_file in enumerate(ann_files):
        print("Reading ", index+1, ":", ann_file)
        gold_anns = read_annotations(join(args.gold, ann_file))
        pred_anns = read_annotations(join(args.pred, ann_file))
        # Remove annotations that are within extraneous spans
        pred_anns, gold_anns = filter_annotations(pred_anns, gold_anns)
        total_gold += len(gold_anns)
        total_pred += len(pred_anns)
        f_cor, f_inc = test_res(pred_anns, gold_anns, rfile, ann_file)
        correct += f_cor
        incorrect += f_inc
    rfile.close()
    precision = 1.0 * correct / (total_pred + 0.000001)
    recall = 1.0 * correct / (total_gold + 0.000001)
    f1sc = 2.0 * precision * recall / (precision + recall + 0.000001)
    print("correct:{}, incorrect:{}, total_gold:{}, total_pred:{}".format(correct, incorrect, total_gold, total_pred))
    print("Precision:{}, Recall:{}, F1:{}".format(precision, recall, f1sc))

def main():
    '''Main method : parse input arguments and test'''
    parser = argparse.ArgumentParser()
    # Input and Output paths
    parser.add_argument('-p', '--pred', type=str, default='out/',
                        help='path to dir where prediction outputs are stored')
    parser.add_argument('-g', '--gold', type=str, default='data/test/',
                        help='path to dir where gold standard labels are stored')
    parser.add_argument('--log', type=str, default="runs/",
                        help='Log directory for error analysis')
    args = parser.parse_args()
    print(args)

    test(args)

if __name__ == '__main__':
    main()
