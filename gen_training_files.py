'''Create smaller efficient embeddings'''
from __future__ import print_function

import sys
import re
import argparse
import pickle
import random
from os import listdir, makedirs
from os.path import join, exists
import codecs
import numpy as np
from gensim.models import Word2Vec
from gensim.models.keyedvectors import KeyedVectors
from segtok.segmenter import split_multi, split_single

from utils import tokenize_document, read_annotations
from utils import TOKEN_B, TOKEN_I, TOKEN_O, CEMB_SIZE
from utils import UNK_FILENAME, NUM_FILENAME, LOC_ANN_TAG, PRO_ANN_TAG
from utils import WORDEMB_FILENAME, CHAREMB_FILENAME
from train import TRAIN_FILE_NAME, VALID_FILE_NAME, TEST_FILE_NAME

def load_data(args, txt_files, outfile):
    """load corpus data and write to IOB formatted files"""
    vocab = set()
    char_lists = []
    max_sent_len = -1
    rfile = codecs.open(join(args.work_dir, outfile), 'w', 'utf-8')
    for _, txt_file in enumerate(txt_files):
        print("Reading", txt_file)
        doc_sents, file_vocab = tokenize_document(txt_file)
        vocab = vocab.union(file_vocab)
        annotations = read_annotations(txt_file[:-3]+"ann")
        for doc_sent in doc_sents:
            empty_sent = True
            for token in doc_sent:
                ignore_token = False
                for ann in annotations:
                    if token.start >= ann.start and token.end <= ann.end:
                        # Protein annotations are sections that can be ignored
                        if ann.atype == PRO_ANN_TAG:
                            ignore_token = True
                        # IOB annotations
                        if ann.atype == LOC_ANN_TAG:
                            if token.start == ann.start:
                                token.encoding = TOKEN_B
                            else:
                                token.encoding = TOKEN_I
                        break
                if not ignore_token:
                    # str_to_print = token.text + "\t" + str(token.start) + "\t" + str(token.end) + "\t" + token.encoding
                    str_to_print = token.text + "\t" + token.encoding
                    print(str_to_print, file=rfile)
                    empty_sent = False
            # print empty line to indicate end of training instance
            if not empty_sent:
                print("", file=rfile)
                if len(doc_sent) > max_sent_len:
                    max_sent_len = len(doc_sent)
                    # print([token.text for token in doc_sent])
        with codecs.open(txt_file, 'r', 'utf-8') as tfile:
            doc_text = tfile.read()
            # Remove Non-ascii characters and new lines
            doc_text = re.sub(r'[^\x00-\x7F]+', '', doc_text)
            doc_text = re.sub(r'[\n]+', ' ', doc_text)
            doc_chars = [char for char in doc_text]
            char_lists.append(doc_chars)
    rfile.close()
    print("Max sent len:", max_sent_len)
    return vocab, char_lists

def create_embeddings(args):
    '''Create embeddings object and dump pickle for use in subsequent models'''
    dataset_vocab = set()
    dataset_char_lists = []
    # Load training set
    txt_files = [join(args.train_corpus, f) for f in listdir(args.train_corpus) if f.endswith(".txt")]
    # calculate number of validation set files and split
    num_valid = int(len(txt_files)*args.valid_perc)
    valid_files, train_files = txt_files[:num_valid], txt_files[num_valid:]
    # Load training data
    vocab, char_lists = load_data(args, train_files, TRAIN_FILE_NAME)
    dataset_vocab = dataset_vocab.union(vocab)
    dataset_char_lists += char_lists
    # Load validation data
    vocab, char_lists = load_data(args, valid_files, VALID_FILE_NAME)
    dataset_vocab = dataset_vocab.union(vocab)
    dataset_char_lists += char_lists
    # Load test data
    txt_files = [join(args.test_corpus, f) for f in listdir(args.test_corpus) if f.endswith(".txt")]
    vocab, char_lists = load_data(args, txt_files, TEST_FILE_NAME)
    dataset_vocab = dataset_vocab.union(vocab)
    dataset_char_lists += char_lists
    print("Total vocab:", len(dataset_vocab), "Total char lists", len(dataset_char_lists))
    print("Loading word embeddings:", args.emb_loc)
    unk_words = set()
    wvec = KeyedVectors.load_word2vec_format(args.emb_loc, binary=True)
    wemb_dict = {}
    for word in dataset_vocab:
        try:
            wemb_dict[word] = wvec[word]
        except KeyError:
            unk_words.add(word)
    print("Number of unknown words:", len(unk_words))
    if not exists(args.work_dir):
        makedirs(args.work_dir)
    # Dump dictionary pickle to disk
    print("Dumping training files to", args.work_dir)
    pickle.dump(wemb_dict, open(join(args.work_dir, WORDEMB_FILENAME), "wb"))
    # Create unk and num word embeddings based on the average of a subsample
    # from the loaded word embeddings and write them to disk
    word_count = 50000  # number of subsample words to process for averaging
    num_count = 10000   # number of sumsample numbers to process for averaging
    # Initialize with a random embeddings for corner cases
    unk = [[random.random() for _ in range(wvec.vector_size)]]
    num = [[random.random() for _ in range(wvec.vector_size)]]
    for word in wvec.vocab:
        if word.isdigit() and num_count > 0:
            num.append(wvec[word])
            num_count -= 1
        elif word_count > 0:
            unk.append(wvec[word])
            word_count -= 1
    unk = np.average(np.asarray(unk), axis=0)
    num = np.average(np.asarray(num), axis=0)
    pickle.dump(unk, open(join(args.work_dir, UNK_FILENAME), "wb"))
    pickle.dump(num, open(join(args.work_dir, NUM_FILENAME), "wb"))
    print("Done!")

    # Char embeddings section
    cemb_dict = {}
    cvec = Word2Vec(dataset_char_lists, size=CEMB_SIZE, min_count=1, workers=6)
    print("Creating character embeddings in", args.work_dir)
    for key, _ in cvec.wv.vocab.items():
        cemb_dict[key] = cvec[key]
    unk = np.array([random.random() for _ in range(CEMB_SIZE)])
    cemb_dict["UNKK"] = unk
    pickle.dump(cemb_dict, open(join(args.work_dir, CHAREMB_FILENAME), "wb"))


def main():
    '''Main method : parse input arguments and train'''
    parser = argparse.ArgumentParser()
    # Input and Output paths
    parser.add_argument('-t', '--train_corpus', type=str, default='data/train/',
                        help='path to dir where training corpus files are stored')
    parser.add_argument('-s', '--test_corpus', type=str, default='data/test/',
                        help='path to dir where test corpus files are stored')
    parser.add_argument('-v', '--valid_perc', type=float, default=0.10,
                        help='proportion of training data for validation')
    parser.add_argument('-e', '--emb_loc', type=str,
                        default='resources/wikipedia-pubmed-and-PMC-w2v.bin',
                        help='path to the word2vec embedding location')
    parser.add_argument('-w', '--work_dir', type=str, default='resources/',
                        help='working directory containing resource files')
    args = parser.parse_args()
    print(args)
    create_embeddings(args)

if __name__ == '__main__':
    main()
